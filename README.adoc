= How to get PDFs

++++
<link rel="stylesheet"  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.1.0/css/font-awesome.min.css">
++++

:icons: font

The format of the current files is `.adoc`.
If you wan to generate a PDF out of the files, you need to follow this guide.

== Prerequisites

All that’s needed is Ruby 2.3 or better.
To download Ruby for your operating system https://www.ruby-lang.org/bg/downloads/[click here]

After you download Ruby you will need to install the asciidoc plugins.
To do so run the following commands:

. Make sure you're using the right encoding UTF-8.
To set the encoding to UTF-8 run:
+
[source, shell]
----
chcp 65001
----

. Install the Asciidoc and  gem.
+
[source, shell]
----
gem install asciidoctor-pdf --pre
----

== Compiling the asciidoc file

To convert the asciidoc to pdf go to the directory of the file where the document is and simply run:

[source, shell]
----
cd P:\ath\to\document.adoc
asciidoctor-pdf document.adoc
----

INFORMATION: If you experience any issues with compiling asciidocs, please visit https://asciidoctor.org/docs/asciidoctor-pdf/
for more information.