:icons: font
:imagesdir: img/
:source-highlighter: rouge

= Introduction to Linux. Running basic bash script commands.

image::linux_terminal.jpg[align=center]

== It is the OS you're using without knowing

Yes, Windows has a huge market share and is the operating system you're used to seeing on computers.
The other operating system most of you know for sure is MacOS which https://hackintosh.com/[supposedly runs] only on Apple computers or so-called Macs.

You most probably heard about Linux as well.
It's gaining more and more popularity these days and that's for reason.
If not you at least heard about it and saw how to install it back in our first exercise.

Now what we're about to tell you, for quite a lot of you is not going to be a surprise, but you actually might be using the Linux operating system right now. If your smartphone is anything different than iPhone and is not a Nokia from 2012-2016, you're actually running Linux right now! Yes, anything that has the word "smart" in it is running on Linux! And here's why...

. *Linux is free*.
For the most part, Linux-based operating systems are completely free for use and distribution.
This allows smart device manufacturers to create devices at a lower cost.
Installing a paid software like Windows on devices forces manufacturers to set a higher cost to their products.
Compare the cost of a laptop with Windows versus a laptop without Windows and you'll see what we're talking about.

. *Linux is open-sourced*.
And what that means is the code for Linux which runs on each and every machine is public and accessible to anyone.
This has its pros and cons, but as open-source driven software gets more and more mainstream the pros begin to outshine the cons.
+
.Here is some comparison...
[TIP]
====
To see why open source matters we'll list some of the many reasons.

|===

|Linux (Open-sourced) |Windows (Close sourced)

|Developed by thousands of people around the world.
|Developed by a dozen of people working at Microsoft.

|Any vulnerabilities, performance issues and driver support is discoverable for anyone and can be fixed by anyone.
This is proven to make more secure software.
|All problems are to be discovered by the employees at the company and fixed by them.
This slows down the time for finding and fixing severe problems, which work as a powerful weapon for hackers.

|Any changes in the code are driven and implemented by the community.
The same people who use the software decide the changes.
|Any changes to the code are decided by the company.
Only Microsoft decides what goes and what does not go into their product.
They can listen to customer feedback and implement changes corresponding to that, but it's up to the company whether they want to listen or not.

|Free for use, modification, and distribution.
For most of the part, open-source software is free to use.
The user does not need to pay for a license and have the ability to do whatever they want with the software.
On the other hand, they might not have support which means that any issues with the software are to be maintained by the user.
|The user needs to pay for a license to legally use the software.
This makes the cost of usage and maintenance higher, which most companies would rather want to cut from their products and most users not to pay for.
However, in most cases, paid products look more complete, whereas free products might look buggy and might not quite suit the needs of the user.

|Open-source software is transparent.
You get and use what you see, you know how your data is used.
|Close-sourced software has control on all of your data and you never know what is collected and how is it used by the company.

|===
====

. *Linux is more secure*.
As an open-source software, millions of people have access to the code and are able to spot and patch vulnerabilities much faster.
This makes it harder for viruses to get installed or hackers to find their way into computers running linux.

. *It is the standard in enterprises offering software services*.
So, Linux is free, open and secure.
This makes the perfect recipe for the base of any software driven company.
- Linux runs on smart devices - Linux-based operating systems are optimized to work on any shape and size of hardware.
From the chips on your SIM card to the most powerful supercomputers.
- Linux is self-sustainable - with the powerful scripting a device can run for days without running into any trouble.
One can program the device to maintain itself and deliver updates to the running software without even needing a restart.
- Linux is unhackable (for the most part) - viruses usually target the masses, which means Windows users.
Their PC user base cannot compare with that of Linux's user base.
This doesn't mean that there are no viruses and security breaches on Linux.
There are, but being an open-source, community driven operating system, allows for those breeches to be quickly found and fixed.
The OS is also designed in such a way that users cannot do harm to it unless they sure know what they are doing.
- Linux is easier and faster to install and deploy - with the power of shell scripting Linux happens to be a great cookie cutter for batch deployment of the same OS configuration on thousands of devices simultaneously.
This makes the OS perfect not only for "smart" hardware but also for hosting services online and creation of distributed systems (clouds).
Most of the websites you visit run on Linux.

image::linux_clouds.jpg[align=center]

Having those reasons and many, many more, we can conclude that Linux-based operating systems deserve a strong hold on the IT market share.
This doesn't mean that Windows should be blown out of the way.
As a close-sourced software Windows has been and still happens to be a more complete, mature and user-friendly operating system, that _just works_.
Big enterprises would still prefer using Windows' Active Directory, as there isn't any Linux solution that will allow that easy level of corporate computer management.

And... the average user is not completely ready to switch to Linux yet.
The main reason for this is that there still isn't that much good end user software available on Linux.
There is no Microsoft Office or PhotoShop for Linux.
There are alternatives but they are not as good.
Most of the computer games are still being written only for Windows and MacOS as well. 
And yes it is possible to install and run Windows software on Linux through Wine, but how many people are going to invest their time, trying to figure out how to make something run on an operating system it is not supposed to?
Although this is starting to change, it wull be time for Linux to become the main operating system.

== GNU Not Unix

You might wonder if Linux is the operating system we're going to use, then why did we install Fedora Server?
Or why is there Ubuntu, Fedora, Debian, Android etc. Why is there no just Linux?

Well Linux is not actually the operating system.
Linux is a kernel.
The kernel is the core of the operating system or simply said if we compare an operating system to a sandwich, the kernel is the butter of that sandwich.
What this means is that Linux helps for the communication between software and hardware.
It is responsible for all the input/output operations inside the operating system.

What the operating system is, is called https://www.gnu.org/gnu/linux-and-gnu.html[*GNU Linux*].
GNU is a recursive abbreviation meaning *GNU Not Unix*.
The reason for this name is, because most non-Windows operating systems such as MacOS, BSD, Solaris and all Linux-based systems origin from https://en.wikipedia.org/wiki/Unix[Unix], meaning they operate in a similar way and have many similarities, just like relatives look alike.

Going further what we call Android, Fedora, Ubuntu, etc. is an operating system built over GNU Linux.
The main difference between them all is the selection of software the community has decided to put inside the operating system.
Other than that it is the same GNU Linux beneath.
And that's why people still refer to Linux.
No matter what you use, you're running the same thing.

== Let's jump right into it!

Having clarified what Linux is and why it is not just Linux we can now start with the basics.

IMPORTANT: To continue further you would need to make sure you have installed Fedora Server in your virtual machine.
If you haven't done so, please go back to exercise 1, install and come back here.

For the most part Linux servers are operated by commend line interface.
Not because there is no GUI, there are many GUIs for Linux, but running commands from the shell happens to be the most efficient, lightweight and fast way to work on a server.
It is also the better option to control a server remotely as the only transmission that happens over the network consists of text.

NOTE: We are going to look into some GUI solutions for Linux server such as https://cockpit-project.org/[Cockpit] and Desktop UIs as well in further exercises.
For now we'll stick with CLIs as they are the standard way to operate a linux server.

The main scripting language Unix-based systems use is https://www.gnu.org/software/bash/[Bash].

Every time you boot into Fedora Server or any other Linux server, you will be asked to log into your user account.

image::login_root.jpg[align=center]

Let's login with the `root` user.
Type `root` next to `login:` and press enter.
You then will be prompted to enter a password.
[.underline]#*Keep in mind that when you type passwords, nothing will show on the screen!*#
Enter the root password you created when setting up your Linux server in the first exercise and press enter.

CAUTION: Although we are using the `root` user in this exercise for our ease, it is strongly recommended not to use the that user for most of the operating system management. 
This user has *full control* over the OS and can make irreversible changes or allow the installation of malicious software.
In the next exercise we are going to create a basic user and use his account for managing the server from then on.
[.underline]*Always use the `root` user responsible and with caution!*

Upon successful login, the screen should look like this:

image::root_successful_login.jpg[align=center]

Just like in PowerShell, at the start we have some information about:

- Who is logged in - `root`
- Where is he logged in - `@localhost` (if logged in from remote server would say the ip address or the domain name of the server).
- What directory is the user in - `~` (`~` refers to the `home` directory of the user. Each Linux user has their own directory where their content is stored.
Anything outside of that is considered as shared and is owned by the system for most part.)

To see the full path of the folder you are in, or the path of a file in this folder, type:

[source, bash]
----
# When you want to see the path of the current folder

readlink -f .

# Or...

realpath .

#When you want to see the path of a file in this folder

readlink -f ./file.ext

# Or...

realpath ./file.ext

----

Currently what you are going to get as an output is `/root`. 
This obviously is the home folder for the `root` user.
All other users' folders lay beneath `/home/<name_of_user>` but more on that later.

=== Browsing the Linux server directories

Just like we did in PowerShell we are going to use the same commends for browsing the Linux server:

- `cd` - changes directory.
It can be relative or absolute with the only difference that in linux we use forward slash ("/") instead of backwards slash for the paths.
- `ls` - lists the contents of a directory.
- `ls -la` - lits the contents of a directory but also gives more metadata about the files, such as creation date, permissions, size and owner.
It also shows hidden files and folders.

TIP: If you want to hide a file or folder in Linux, you just need to name it starting with a "dot" (e.g. `.very-important-homework`, `.crypto-wallet.txt`), but keep in mind that `ls -la` or `ll` commend for short will expose those files. 

=== Understanding Linux directories

Unlike Windows, nix (short for Unix-based) operating systems have a bit different file structure system.
The main folder on the system is "/" which is the equivalent of `Local Disk C:`, but unlike `C:`, this is the root of everything. Every device, folder and content on the server is located somewhere inside a folder beneath "/".

If you are still at `/root`, let's move to `/` and see what's inside...

[source, bash]
----
cd /
ls -lha
----

image:root_folders.jpg[align=center]

NOTE: Not to get confused the with the first line here - `total 16` does not mean there are 16 files and folder, neither does mean the folders are 16 (in fact they are 15 listed here) .
It means the total number of blocks taken on the disk by those files.

As you can see there are multiple files listed, most of which folders.

- The first column shows the file's *permissions*:
-- `d` - the file is a directory
-- `r` - the file can be read
-- `w` - the file can be written to
-- `x` - the file can be executed (if executable)
-- `t` - the file can be only deleted or renamed only by it's creator
- The next column shows the *number of links* - these are the number of places where a shortcut to that directory's been made.
- After that is the *username of the owner* (`root` in these cases).
- The next that follows is the *name of the group*, the file is shared with.
If the name equals the owner, this means the file is not owned by a group.
- The fifth column shows *the size of the file*
- Next columns show the *date and tme of last modification*
- And the final column shows the *name of the file*, where the color differs according to the permissions or the file type and folders like `sbin -> usr/sbin` are links, showing where they lead to.

TIP: Read https://www.howtogeek.com/117435/htg-explains-the-linux-directory-structure-explained/[this article] to learn more about what each folder is for.

=== Creating files and directories

Let's `cd` back to `/root` (use `cd ~` for short) and see how we can create folders and files.

[NOTE]
====
For file creation will use `vim` as a preferred editor.
Fedora Server does not have `vim` installed by default so we'll need to do it ourselves. 

Run:

[source, bash]
----
dnf install -y vim
----

We are going to look more into installing software in further exercises.
====

To create a folder you just need to use the `mkdir` command.

[source, bash]
----
mkdir "My cool folder"
----

Now if we try to execute `ls`, we should be able to see our folder listed.

WARNING: Keep in mind that in Linux names are case sensitive, which means that you can create multiple files with the same name but have them in different combination of upper and lower case letters.
Be careful when you change directories, entering the exact name just as it is.

Creating files on Linux can be done in several ways:

- You can use the inbuilt `cat` command
+
[source, bash]
----
# '>' depicts the output. If not set will output in the console 
cat > my_file.txt
----

- You can set the output of a program to be written on a file
+
[source, bash]
----
my_program &> log.txt
----

- You can use an editor to have a more advanced control over the files. In our example we will use `vim`.
+
[source, bash]
----
vim my_text_file.txt
----

Upon opening of `vim` you'll be met with the most intimidating screen of all.
Blank black screen doing nothing against your input and not allowing you to exit!

image::vim_when_opened.jpg[align=center]

Don't be afraid `vim` is fairly easy to use.
If you want to close it, you can use some of the steps below:

image::close_vim.jpg[align=center]

To start off, make sure that the screen you're seeing looks just like te blank one above and has no other text than the lines on the bottom right that say `0,0-1 All`.
If that's not the case, press `esc`.

. Press `i` key on your keyboard and make sure that the bottom left corner of the screen says `-- INSERT --`. That is what `i` stands for.
+
image::vim_insert.jpg[align=center]
+
Now you should be able to type whatever you want on the screen.
You can use your arrow keys to move left, write, up and down with the cursor.

. When you're done writing press `esc`. 
Make sure that `-- INSERT --` is gone and nothing is displayed, but your text.

. Type `:wq` to exit
- `:` denotes that you are going to run a command
- `w` means you want to *write* the file
- `q` means you want to *quit* the editor

When you exit, you should see your bash session again.

=== Copying files

Copying happens with the `cp` command.

[source, bash]
----
# Copy a file
cp ./file.txt /new/dest


# Copy and rename a file
cp ./file.txt /new/dest/file-copy.txt

# Copy a directory
cp -r ./directory /new/dest
----

=== Moving and remaming files on linux.

If you want to move or rename a file of folder on linux use the `mv` command.

[source, bash]
----
#Renames a file or folder
mv file.txt new_name.txt

#Moves a file. /var/log is destination directory
mv ./file.txt /var/log

#Moves and renames a file
mv ./file.txt /var/log/new-file.txt

#Moves a directory.
mv -r ./directory /path/to/new/place
----

=== Deleting files on Linux

Deletion is done through the `rm` command.

[source, bash]
----
# You have to agree every time that you want to remove the file
rm file.txt

# Will remove the file without asking for confirmation
rm -f file.txt

# Folders must be removed with the '-r' argument
rm -rf "my directory"
----

=== Getting help

Just as with PowerShell to get help you can use the `help` command or the `man` commend (recommended).

You can also look for resources on the web.
The Linux community is full of humble and nice people happy to help and give you the knowledge you need.

image::getting_linux_help.png[align=center]


=== Powering off or restarting the machine

Finally you would want a way to shutdown the computer.

You can do so by using the `shutdown` command:

[source, bash]
----
shutdown -P now
----

The `-P` argument is used to schedule the time of shutdown.
If you execute just `shutdown`, the computer will shutdown after a minute.

Restarting is done with the `reboot` command.
Just type `reboot`, and your PC will reboot.